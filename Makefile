.PHONY: clean
clean :
	rm -rf src/*.pyc
	rm -rf docs/_build
	rm -rf venv
	rm -rf venv3
	rm -f .coverage

venv:
	virtualenv venvvenv_venv_
	ln -s venv/bin/activate venv_activate

venv3:
	virtualenv venv3 -p /usr/bin/python3
	ln -s venv3/bin/activate venv3_activate
