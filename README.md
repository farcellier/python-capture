# Capture

python script to save a screenshot in a file (``file.png``).

## The latest version

You can find the latest version to ...

    git clone git@bitbucket.org:farcellier/python-capture.git

## Code Example

Call the tool

    python capture.py

## Motivation

A short description of the motivation behind the creation and maintenance of the project. This should explain **why** the project exists.

## Installation

### Ubuntu

Ensure you have libjpeg and zlib on your computer.

    apt-get install libjpeg-dev zlib1g-dev

Active the virtual env

    make venv3
    source venv3_activate

To install the requirements to run the project

    pip install -r requirements.txt

## Contributors

* Fabien Arcellier

## License

A short snippet describing the license (MIT, Apache, etc.)
