#!/usr/bin/python

from PIL.Image import Image
import pyscreenshot
import sys

def main(args):
    im=pyscreenshot.grab() # type: Image

    im.thumbnail((800, 600))
    im.save('file.png')

if __name__ == "__main__":
    main(sys.argv)
